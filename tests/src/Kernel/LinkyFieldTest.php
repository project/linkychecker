<?php

namespace Drupal\Tests\linkychecker\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests additional linky fields.
 *
 * @group linkychecker
 */
class LinkyFieldTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'linky',
    'dynamic_entity_reference',
    'link', 'field',
    'user',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installSchema('user', 'users_data');
    $this->installEntitySchema('linky');
  }

  /**
   * Covers linkychecker_entity_base_field_info().
   */
  public function testLinkyFieldsCreation() {
    // Additional fields does't exist before 'linkychecker' is installed.
    $this->assertTrue(empty($this->container->get('entity.definition_update_manager')->needsUpdates()['linky']));
    $this->assertTrue(empty($this->container->get('entity_field.manager')->getFieldStorageDefinitions('linky')['http_status']));
    // Install 'linkychecker'.
    $this->container->get('module_installer')->install(['linkychecker']);
    // There should be no updates as 'linkychecker' is installed.
    $this->assertTrue(empty($this->container->get('entity.definition_update_manager')->needsUpdates()['linky']));
    $this->assertTrue(!empty($this->container->get('entity_field.manager')->getFieldStorageDefinitions('linky')['http_status']));
    // Uninstall the module.
    $this->container->get('module_installer')->uninstall(['linkychecker']);
    // There should be no updates and basefields provided by linkychecker
    // should be gone.
    $this->assertTrue(empty($this->container->get('entity.definition_update_manager')->needsUpdates()['linky']));
    $this->assertTrue(empty($this->container->get('entity_field.manager')->getFieldStorageDefinitions('linky')['http_status']));
  }

}
