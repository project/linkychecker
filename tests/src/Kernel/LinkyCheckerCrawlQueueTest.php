<?php

namespace Drupal\Tests\linkychecker\Kernel;

use Drupal\linky\Entity\Linky;
use Drupal\Tests\linky\Kernel\LinkyKernelTestBase;

/**
 * Defines a test for LinkyCheckerCrawlQueue.
 *
 * @covers \Drupal\linkychecker\LinkyCheckerCrawlQueue
 * @group linkychecker
 */
class LinkyCheckerCrawlQueueTest extends LinkyKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'linky',
    'dynamic_entity_reference',
    'link',
    'field',
    'linkychecker',
    'user',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  public function setup(): void {
    parent::setUp();
    $this->installSchema('user', 'users_data');
    $this->installEntitySchema('linky');
    $this->installConfig(['linkychecker']);
  }

  /**
   * Tests older items are crawled first.
   */
  public function testLinkyCheckerCrawlQueueTest() {
    $link1 = Linky::create([
      'link' => [
        'uri' => 'http://example.com',
        'title' => 'Example.com',
      ],
      'checked' => 1,
    ]);
    $link1->save();
    $link2 = Linky::create([
      'link' => [
        'uri' => 'http://example.com/foo',
        'title' => 'Example.com (Foo)',
      ],
      'checked' => 2,
    ]);
    $link2->save();
    \Drupal::service('linkychecker.crawl_queue')->jobCreator();
    $item = \Drupal::queue('linkychecker_crawl_job')->claimItem();
    $this->assertEquals([$link1->id(), $link2->id()], $item->data['linky_ids']);
  }

}
