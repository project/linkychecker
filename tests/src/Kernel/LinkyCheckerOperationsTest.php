<?php

namespace Drupal\Tests\linkychecker\Kernel;

use Drupal\linky\Entity\Linky;
use Drupal\Tests\linky\Kernel\LinkyKernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Tests linkychecker operations.
 *
 * @group linkychecker
 */
class LinkyCheckerOperationsTest extends LinkyKernelTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'linky',
    'dynamic_entity_reference',
    'link',
    'field',
    'linkychecker',
    'user',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  public function setup(): void {
    parent::setUp();
    $this->installSchema('user', 'users_data');
    $this->installEntitySchema('user');
    $this->installConfig(['linkychecker']);

    // Avoid user 1.
    $this->createUser();
  }

  /**
   * Tests linkychecker operations access control.
   */
  public function testLinkyCheckerOperations(): void {
    $listBuilder = \Drupal::entityTypeManager()->getListBuilder('linky');

    $normalUser = $this->createUser();
    $owner = $this->createUser([
      'edit own linky entities',
    ]);
    $editAny = $this->createUser([
      'edit linky entities',
    ]);
    $excludedLinky = Linky::create([
      'link' => [
        'uri' => 'http://example.com',
        'title' => 'Example.com',
      ],
      'excluded' => 1,
    ]);
    $excludedLinky->save();
    $includedLinky = Linky::create([
      'link' => [
        'uri' => 'http://example.com',
        'title' => 'Example.com',
      ],
      'excluded' => 0,
    ]);
    $includedLinky->save();

    \Drupal::currentUser()->setAccount($normalUser);
    $this->assertEmpty($listBuilder->getOperations($excludedLinky));
    $this->assertEmpty($listBuilder->getOperations($includedLinky));

    \Drupal::currentUser()->setAccount($editAny);
    $includedOperations = $listBuilder->getOperations($includedLinky);
    // Non-excluded linky gets crawl and exclude, and not include.
    $this->assertArrayHasKey('crawl', $includedOperations);
    $this->assertArrayHasKey('exclude', $includedOperations);
    $this->assertArrayNotHasKey('include', $includedOperations);

    $excludedOperations = $listBuilder->getOperations($excludedLinky);
    // Excluded linky gets include and not exclude or crawl.
    $this->assertArrayHasKey('include', $excludedOperations);
    $this->assertArrayNotHasKey('crawl', $excludedOperations);
    $this->assertArrayNotHasKey('exclude', $excludedOperations);

    // Operations aren't added if user cannot edit.
    \Drupal::currentUser()->setAccount($owner);
    $this->assertEmpty($listBuilder->getOperations($excludedLinky));
    $this->assertEmpty($listBuilder->getOperations($includedLinky));

    // Setting owner should grant operations.
    $excludedLinky->setOwner($owner)->save();
    $includedLinky->setOwner($owner)->save();
    \Drupal::entityTypeManager()->getAccessControlHandler('linky')->resetCache();
    $includedOperations = $listBuilder->getOperations($includedLinky);
    // Non-excluded linky gets crawl and exclude, and not include.
    $this->assertArrayHasKey('crawl', $includedOperations);
    $this->assertArrayHasKey('exclude', $includedOperations);
    $this->assertArrayNotHasKey('include', $includedOperations);

    $excludedOperations = $listBuilder->getOperations($excludedLinky);
    // Excluded linky gets include and not exclude or crawl.
    $this->assertArrayHasKey('include', $excludedOperations);
    $this->assertArrayNotHasKey('crawl', $excludedOperations);
    $this->assertArrayNotHasKey('exclude', $excludedOperations);
  }

}
