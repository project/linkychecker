<?php

namespace Drupal\Tests\linkychecker\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests linky checker recheck form.
 *
 * @group linkychecker
 */
class LinkyCheckerReCheckFormTest extends BrowserTestBase {

  /**
   * The admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  public static $modules = ['linkychecker'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Permissions to grant admin user.
   *
   * @var array
   */
  protected $permissions = [
    'access administration pages',
    'recheck linkychecker',
    'add linky entities',
    'edit linky entities',
    'view linky entities',
  ];

  /**
   * Sets the test up.
   */
  protected function setUp(): void {
    parent::setUp();
    // Test admin user.
    $this->adminUser = $this->drupalCreateUser($this->permissions);
  }

  /**
   * Test that user can use the linkychecker recheck form.
   */
  public function testLinkyCheckerRecheck() {
    $assertSession = $this->assertSession();
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/config/content/linkychecker/recheck');
    $assertSession->statusCodeEquals(200);
    $assertSession->buttonExists('Recheck all Manage Links');
    $this->click('#edit-recheck');
    $assertSession->responseContains('0 jobs created for 0 links.');

    $linkStorage = \Drupal::entityTypeManager()->getStorage('linky');
    $link1 = $linkStorage->create([
      'link' => [
        'uri' => 'http://example.com',
        'title' => 'Example.com',
      ],
    ]);
    $link1->save();
    $this->drupalGet('/admin/config/content/linkychecker/recheck');
    $assertSession->statusCodeEquals(200);
    $assertSession->buttonExists('Recheck all Manage Links');
    $this->click('#edit-recheck');
    $assertSession->responseContains('1 jobs created for 1 links.');

    $this->drupalGet('/admin/config/content/linkychecker/recheck');
    $assertSession->statusCodeEquals(200);
    $assertSession->buttonExists('Recheck all Manage Links');
    $this->click('#edit-recheck');
    $assertSession->responseContains('No jobs created because there are still jobs in the queue.');

  }

}
