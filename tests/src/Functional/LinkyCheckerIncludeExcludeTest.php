<?php

namespace Drupal\Tests\linkychecker\Functional;

use Drupal\linky\LinkyInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests linky exclusion/inclusion from link checking.
 *
 * @group linkychecker
 */
class LinkyCheckerIncludeExcludeTest extends BrowserTestBase {

  /**
   * The admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['linkychecker'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Permissions to grant admin user.
   *
   * @var array
   */
  protected $permissions = [
    'access administration pages',
    'add linky entities',
    'edit linky entities',
    'view linky entities',
  ];

  /**
   * Sets the test up.
   */
  protected function setUp(): void {
    parent::setUp();
    // Test admin user.
    $this->adminUser = $this->drupalCreateUser($this->permissions);
  }

  /**
   * Checks linky exclusion/inclusion.
   */
  public function testLinkExclusionInclusion() {

    $linkStorage = \Drupal::entityTypeManager()->getStorage('linky');
    $link1 = $linkStorage->create([
      'link' => [
        'uri' => 'http://example.com',
        'title' => 'Example.com',
      ],
    ]);
    $link1->save();
    $link2 = $linkStorage->create([
      'link' => [
        'uri' => 'http://1337.com',
        'title' => '1337.com',
      ],
    ]);
    $link2->save();
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/content/linky');
    $assert_session = $this->assertSession();
    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContains($link1->label());
    $assert_session->pageTextContains($link2->label());
    $this->assertOperationalLink($link1, 1, 3);
    $this->assertOperationalLink($link2, 2, 3);

    $this->clickLink('Exclude', 0);
    $this->submitForm([
      'reason' => 'Example link excluded.',
    ], 'Confirm');

    $link1 = $linkStorage->loadUnchanged($link1->id());
    $this->assertEquals(1, $link1->excluded->value);
    $this->assertSame('Example link excluded.', $link1->reason->value);

    $this->drupalGet('/admin/content/linky');
    $this->assertOperationalLink($link1, 1, 2);
    $this->assertOperationalLink($link2, 2, 3);

    $this->clickLink('Remove Exclusion', 0);
    $this->submitForm([], 'Confirm');
    $link1 = $linkStorage->loadUnchanged($link1->id());
    $this->assertEquals(0, $link1->excluded->value);
    $this->assertEmpty($link1->reason->value);

  }

  /**
   * Asserts number of operation links for given table row.
   *
   * @param \Drupal\linky\LinkyInterface $linky
   *   The current linky.
   * @param int $index
   *   The row number.
   * @param int $number_of_links
   *   The number of operation links.
   */
  public function assertOperationalLink(LinkyInterface $linky, int $index, int $number_of_links): void {
    $operation_links = $this->cssSelect(sprintf('table tbody tr:nth-child(%d) td ul.dropbutton li a', $index));
    $this->assertNotEmpty($operation_links);
    $url = base_path() . "admin/content/linky/{$linky->id()}";
    $number_of_links_found = 0;
    foreach ($operation_links as $link) {
      switch ($link->getText()) {
        case 'Edit':
          $this->assertStringContainsString("$url/edit?destination=", $link->getAttribute('href'));
          $number_of_links_found++;
          break;

        case 'Re-check':
          $this->assertSame("$url/crawl", $link->getAttribute('href'));
          $number_of_links_found++;
          break;

        case 'Exclude':
          $this->assertSame("$url/exclude", $link->getAttribute('href'));
          $number_of_links_found++;
          break;

        case 'Remove Exclusion':
          $this->assertSame("$url/include", $link->getAttribute('href'));
          $number_of_links_found++;
          break;

      }
    }
    $this->assertEquals($number_of_links, $number_of_links_found);
  }

}
