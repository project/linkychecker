Linkychecker

https://www.drupal.org/project/linkychecker

By default Linkychecker will queue link jobs and run the queue in cron.
Alternatively you can disable the cron functionality and run each of the
commands in sequence:

# Tips

```sh
# Create queue items.
drush linkychecker:queue-links
# Process the queue.
drush queue:run linkychecker_crawl_job
```

Because Crawl jobs are queue items and automatically receive locking 
functionality, you can run multiple instances of the queue runner to 
accelerate crawling, for example:

```sh
# Create two concurrent queue runners:
drush queue:run linkychecker_crawl_job & drush queue:run linkychecker_crawl_job
```

# License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
