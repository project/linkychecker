<?php

namespace Drupal\linkychecker\Commands;

use Drupal\linkychecker\LinkyCheckerCrawlQueueInterface;
use Drush\Commands\DrushCommands;

/**
 * Drush commandfile for Linkychecker.
 */
class LinkyCheckerCommands extends DrushCommands {

  /**
   * The linky checker queue service.
   *
   * @var \Drupal\linkychecker\LinkyCheckerCrawlQueueInterface
   */
  protected $checkerCrawlQueue;

  /**
   * Constructs a new LinkyCheckerCommands.
   *
   * @param \Drupal\linkychecker\LinkyCheckerCrawlQueueInterface $checkerCrawlQueue
   *   The linky checker queue service.
   */
  public function __construct(LinkyCheckerCrawlQueueInterface $checkerCrawlQueue) {
    parent::__construct();
    $this->checkerCrawlQueue = $checkerCrawlQueue;
  }

  /**
   * Fills queue with crawler jobs.
   *
   * @command linkychecker:queue-links
   * @aliases lcql
   */
  public function crawlJobCreator() {
    $this->checkerCrawlQueue->jobCreator();
    $this->logger()->success(dt('Jobs queued.'));
  }

}
