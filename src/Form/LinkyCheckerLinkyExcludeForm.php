<?php

declare(strict_types = 1);

namespace Drupal\linkychecker\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Link entity exclusion form.
 *
 * @property \Drupal\linky\LinkyInterface $entity
 */
class LinkyCheckerLinkyExcludeForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('<none>');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Exclude the %label?', [
      '%label' => $this->getEntity()->link->title,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Are you sure you want to exclude the %label from the link checking report?', [
      '%label' => $this->getEntity()->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['reason'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Reason for exclusion'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // ContentEntityConfirmFormBase::actions() don't set ::save so we have to
    // save entity here.
    $this->entity->excluded = 1;
    $this->entity->reason = $form_state->getValue('reason');
    $this->entity->save();
  }

}
