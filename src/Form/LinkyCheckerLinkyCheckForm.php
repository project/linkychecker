<?php

declare(strict_types = 1);

namespace Drupal\linkychecker\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Ajax\AjaxFormHelperTrait;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\linkychecker\LinkyCheckerCrawlerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Linky check form.
 *
 * @method \Drupal\linky\LinkyInterface getEntity()
 */
class LinkyCheckerLinkyCheckForm extends ContentEntityForm {

  use AjaxFormHelperTrait;

  /**
   * Linky checker crawler.
   *
   * @var \Drupal\linkychecker\LinkyCheckerCrawlerInterface
   */
  protected $crawler;

  /**
   * The in-memory logger.
   *
   * @var \Psr\Log\LoggerInterface|\Drupal\linkychecker\LinkyCheckerMemoryLogger
   */
  protected $logger;

  /**
   * Constructs a new LinkyCheckerLinkyCheckForm.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\linkychecker\LinkyCheckerCrawlerInterface $crawler
   *   Linky checker crawler.
   * @param \Psr\Log\LoggerInterface $logger
   *   The in-memory logger.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, LinkyCheckerCrawlerInterface $crawler, LoggerInterface $logger) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->crawler = $crawler;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('linkychecker.crawler.memory'),
      $container->get('logger.linkychecker.memory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'linky_check';
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $linky = $this->getEntity();
    $isError = $linky->error->value ?? NULL;
    $message = $linky->message->value ?? NULL;

    $form['table_help'] = [
      '#markup' => $this->t('Results of last crawl:'),
    ];
    $form['table'] = [
      '#type' => 'table',
      '#rows' => [
        [$this->t('Error?'), $isError ? $this->t('Yes') : $this->t('No')],
        [$this->t('Message'), !empty($message) ? Xss::filterAdmin($message) : $this->t('- No message -')],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $linky = $this->getEntity();
    if ($linky->excluded->value) {
      $form_state->setError($form, $this->t('This link is excluded from the link checking.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $linky = $this->getEntity();

    $this->crawler->checkLinks([$linky]);
    $logs = $this->logger->getLogs();
    foreach ($logs as $log) {
      [$severity, $message, $args] = $log;
      // @codingStandardsIgnoreLine
      $line = $this->t(Xss::filterAdmin($message), $args);
      $this->messenger()->addMessage($line);
    }
    if (!empty($logs)) {
      $isError = $linky->error->value ?? NULL;
      $message = $linky->message->value ?? NULL;
      $tArgs = [
        '@message' => !empty($message) ? $message : $this->t('- No message -'),
      ];
      if ($isError) {
        $this->messenger()->addError($this->t('Crawling resulted in error: @message', $tArgs));
      }
      else {
        if (!empty($message)) {
          $this->messenger()->addMessage($this->t('Crawling resulted in success: @message', $tArgs));
        }
        else {
          $this->messenger()->addMessage($this->t('Crawling resulted in success', $tArgs));
        }
      }
    }
    else {
      $this->messenger()->addWarning($this->t('No output'));
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state): array {
    $actions['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Re-check'),
      '#submit' => ['::submitForm'],
    ];
    if ($this->isAjax()) {
      $actions['submit']['#ajax']['callback'] = '::ajaxSubmit';
    }
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    // Not saving.
  }

  /**
   * {@inheritdoc}
   */
  protected function successfulAjaxSubmit(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $form['status_messages'] = [
      '#type' => 'status_messages',
    ];
    // Open the form again as a modal.
    return $response->addCommand(new OpenModalDialogCommand(
      $this->t('Crawl'),
      $form,
      ['width' => '700']
    ));
  }

}
