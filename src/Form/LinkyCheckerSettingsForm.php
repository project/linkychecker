<?php

namespace Drupal\linkychecker\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Linkychecker settings.
 */
class LinkyCheckerSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'linkychecker_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $linkyCheckerSettings = $this->config('linkychecker.settings');

    $form['enable_cron'] = [
      '#title' => $this->t('Enable cron'),
      '#type' => 'checkbox',
      '#description' => $this->t('Whether to create link crawl jobs during cron.'),
      '#default_value' => $linkyCheckerSettings->get('enable_cron'),
      '#config' => [
        'key' => 'linkychecker.settings:enable_cron',
      ],
    ];

    $form['user_agent'] = [
      '#title' => $this->t('User agent'),
      '#type' => 'textfield',
      '#description' => $this->t('User agent used for crawling.'),
      '#default_value' => $linkyCheckerSettings->get('user_agent'),
      '#config' => [
        'key' => 'linkychecker.settings:user_agent',
      ],
    ];

    $form['crawl_interval'] = [
      '#title' => $this->t('Crawl interval'),
      '#type' => 'number',
      '#field_suffix' => $this->t('seconds'),
      '#description' => $this->t('How long to wait before re-crawling a link. Recommended interval is 1-14 days.'),
      '#default_value' => $linkyCheckerSettings->get('crawl_interval'),
      '#min' => 1,
      '#config' => [
        'key' => 'linkychecker.settings:crawl_interval',
      ],
    ];

    $form['crawl_per_job'] = [
      '#title' => $this->t('Crawls per job'),
      '#type' => 'number',
      '#field_suffix' => $this->t('links'),
      '#description' => $this->t('Number of links to crawl per job. This is primarily used to control memory usage. A low number such as 5 is recommended.'),
      '#default_value' => $linkyCheckerSettings->get('crawl_per_job'),
      '#min' => 1,
      '#config' => [
        'key' => 'linkychecker.settings:crawl_per_job',
      ],
    ];

    $form['retries_before_advanced_crawl'] = [
      '#title' => $this->t('Retries before advanced crawl'),
      '#type' => 'number',
      '#field_suffix' => $this->t('retries before advanced crawl'),
      '#min' => 0,
      '#description' => $this->t('Number of failed crawl attempts before trying more thorough crawling methods.'),
      '#default_value' => $linkyCheckerSettings->get('retries_before_advanced_crawl'),
      '#config' => [
        'key' => 'linkychecker.settings:retries_before_advanced_crawl',
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $this->config('linkychecker.settings')
      ->set('user_agent', $form_state->getValue('user_agent'))
      ->set('crawl_interval', $form_state->getValue('crawl_interval'))
      ->set('crawl_per_job', $form_state->getValue('crawl_per_job'))
      ->set('enable_cron', $form_state->getValue('enable_cron'))
      ->set('retries_before_advanced_crawl', $form_state->getValue('retries_before_advanced_crawl'))
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['linkychecker.settings'];
  }

}
