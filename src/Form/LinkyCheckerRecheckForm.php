<?php

namespace Drupal\linkychecker\Form;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\linkychecker\LinkyCheckerCrawlQueueInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Linkychecker settings.
 */
class LinkyCheckerRecheckForm extends FormBase {

  /**
   * The linky checker queue service.
   *
   * @var \Drupal\linkychecker\LinkyCheckerCrawlQueueInterface
   */
  protected $checkerCrawlQueue;

  /**
   * The in-memory logger.
   *
   * @var \Psr\Log\LoggerInterface|\Drupal\linkychecker\LinkyCheckerMemoryLogger
   */
  protected $logger;

  /**
   * Constructs a new LinkyCheckerCronHook.
   *
   * @param \Drupal\linkychecker\LinkyCheckerCrawlQueueInterface $checkerCrawlQueue
   *   The linky checker queue service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The in-memory logger.
   */
  public function __construct(LinkyCheckerCrawlQueueInterface $checkerCrawlQueue, LoggerInterface $logger) {
    $this->checkerCrawlQueue = $checkerCrawlQueue;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $construct_args = [
      $container->get('linkychecker.crawl_queue.memory'),
      $container->get('logger.linkychecker.memory'),
    ];
    return new static(...$construct_args);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'linkychecker_recheck_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['linkychecker_recheck'] = [
      '#type' => 'details',
      '#title' => t('Recheck Manage Links'),
      '#open' => TRUE,
    ];

    $form['linkychecker_recheck']['recheck'] = [
      '#type' => 'submit',
      '#value' => t('Recheck all Manage Links'),
      '#submit' => ['::submitRecheck'],
    ];
    return $form;
  }

  /**
   * Recheck all Manage Links.
   */
  public function submitRecheck(array &$form, FormStateInterface $form_state) {
    $this->checkerCrawlQueue->jobCreator();
    $logs = $this->logger->getLogs();
    foreach ($logs as $log) {
      [$severity, $message, $args] = $log;
      // @codingStandardsIgnoreLine
      $line = $this->t(Xss::filterAdmin($message), $args);
      $this->messenger()->addMessage($line);
    }
    if (empty($logs)) {
      $this->messenger()->addWarning($this->t('No output'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
  }

}
