<?php

declare(strict_types = 1);

namespace Drupal\linkychecker\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Link entity inclusion form.
 *
 * @property \Drupal\linky\LinkyInterface $entity
 */
class LinkyCheckerLinkyIncludeForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('<none>');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Remove exclusion the %label?', [
      '%label' => $this->getEntity()->link->title,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Are you sure you want to remove the exclusion of %label from the link checking report?', [
      '%label' => $this->getEntity()->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // ContentEntityConfirmFormBase::actions() don't set ::save so we have to
    // save entity here.
    $this->entity->excluded = 0;
    $this->entity->reason = NULL;
    $this->entity->save();
  }

}
