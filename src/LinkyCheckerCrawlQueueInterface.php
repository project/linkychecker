<?php

declare(strict_types = 1);

namespace Drupal\linkychecker;

use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Interface for the linky queue service.
 */
interface LinkyCheckerCrawlQueueInterface {

  /**
   * Name of a state variable name with the last start date.
   *
   * A unix timestamp.
   */
  public const STATE_LAST_START = 'linkychecker_crawl_queue_last_start';

  /**
   * Name of a state variable name with last successful queue item creation.
   *
   * A unix timestamp.
   */
  public const STATE_LAST_SUCCESS = 'linkychecker_crawl_queue_last_success';

  /**
   * Creates queue items for links needing update.
   *
   * To prevent duplication, jobs arnt created until the queue is empty.
   */
  public function jobCreator(): void;

  /**
   * Determine which links need to be added to the crawl queue.
   *
   * @param int $threshold
   *   Set a timestamp for any links older than this will be re-added to the
   *   queue.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   An prepared query.
   */
  public function getLinksNeedingUpdate(int $threshold): QueryInterface;

}
