<?php

namespace Drupal\linkychecker;

/**
 * Interface for the linky checker crawler.
 */
interface LinkyCheckerCrawlerInterface {

  /**
   * Crawls the link and stores the result.
   *
   * @param \Drupal\linky\LinkyInterface[] $links
   *   An array of linky entities.
   */
  public function checkLinks(array $links);

}
