<?php

namespace Drupal\linkychecker;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\linky\LinkyInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RedirectMiddleware;
use Psr\Log\LoggerInterface;

/**
 * The linky checker crawler.
 */
class LinkyCheckerCrawler implements LinkyCheckerCrawlerInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The default http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Service for obtaining system time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new LinkyCheckerCrawler.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \GuzzleHttp\Client $httpClient
   *   The default http client.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Service for obtaining system time.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(ConfigFactoryInterface $configFactory, Client $httpClient, TimeInterface $time, LoggerInterface $logger) {
    $this->configFactory = $configFactory;
    $this->httpClient = $httpClient;
    $this->time = $time;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function checkLinks(array $links) {
    foreach ($links as $link) {
      $this->checkLink($link);
    }
  }

  /**
   * Check the status of a link.
   *
   * @param \Drupal\linky\LinkyInterface $link
   *   A linky entity.
   */
  protected function checkLink(LinkyInterface $link): void {
    $linkyCheckerSettings = $this->configFactory->get('linkychecker.settings');

    $this->logger->info('Crawling link @id', [
      '@id' => $link->id(),
    ]);

    $error = FALSE;
    $message = NULL;

    /** @var \Drupal\link\Plugin\Field\FieldType\LinkItem $item */
    try {
      $item = $link->link->first();
      $url = $item->getUrl();
      $uri = $url->setAbsolute()->toString();

      $parsedUrl = parse_url($uri);
      if ($parsedUrl === FALSE) {
        $error = TRUE;
        $message = sprintf("Invalid URI: %s", $uri);
        $this->logger->debug($message);
      }
    }
    catch (\Exception $e) {
      // Catch everything.
      $error = TRUE;
      $message = sprintf("Exception thrown: %s", $e->getMessage());
      $this->logger->debug($message);
    }

    $quitEarly = $error;
    $requestTime = $this->time->getRequestTime();

    $scheme = $parsedUrl['scheme'] ?? NULL;
    if (!in_array($scheme, ['http', 'https'], TRUE)) {
      $error = FALSE;
      $message = 'Not an allowed scheme.';
      $this->logger->debug($message);
      $quitEarly = TRUE;
    }

    // If there is already an error, then quit.
    if ($quitEarly) {
      // We dont change 'error_count' here because that field is counting http
      // request failures.
      $link->setLastCheckedTime($requestTime);
      $link->http_method = NULL;
      $link->http_status = NULL;
      $link->destination = NULL;
      $link->message = $message;
      $link->error = $error;
      $link->error_count = 0;
      $link->internal = NULL;
      $link->save();
      return;
    }

    $firstRequest = !isset($link->http_method->value);
    $method = $link->http_method->value ?? 'HEAD';
    try {
      $response = $this->request($uri, $method);
    }
    catch (RequestException $e) {
      $response = $e->getResponse();
      $error = TRUE;
      $message = sprintf('[[uri: %s]]: %s', $uri, $e->getMessage());
      $this->logger->debug($message);
    }
    catch (\Exception $e) {
      $error = TRUE;
      $message = sprintf('Fatal exception: %s', $e->getMessage());
      $this->logger->debug($message);
    }

    $errorCount = $link->error_count->value ?? 0;
    $errorThreshold = $linkyCheckerSettings->get('retries_before_advanced_crawl');
    assert(is_int($errorThreshold));

    // If the HEAD request failed, and it was the first request OR there were
    // previous consecutive failures, then try again with GET.
    if ($error && ($firstRequest || ($errorCount >= $errorThreshold))) {
      try {
        // Try again, but with GET method instead, as some web servers and apps
        // don't respond to HEAD appropriately.
        $response = $this->request($uri, 'GET');
        // If an exception wasn't thrown, then change the method for subsequent
        // requests.
        $method = 'GET';
        $error = FALSE;
        $message = 'HEAD was attempted first but failed. GET resulted in a successful response';
        $this->logger->debug($message);
      }
      catch (RequestException $e) {
        // If we got an error again, just use the error details from previous
        // HEAD request.
        $message .= ' (GET was also attempted)';
        $this->logger->debug($message);
      }
      catch (\Exception $e) {
        $error = TRUE;
        $message = sprintf('Fatal exception: %s', $e->getMessage());
        $this->logger->debug($message);
      }
    }

    $status = NULL;
    $destination = NULL;

    // Response may be null if the request failed.
    if (isset($response)) {
      /** @var string[] $history */
      $historyUri = $response->getHeader(RedirectMiddleware::HISTORY_HEADER);
      if (!count($historyUri)) {
        // The response was returned without redirects.
        $destination = NULL;
        $status = $response->getStatusCode();
      }
      else {
        // There were redirects from the original requested URI.
        $error = TRUE;
        $historyStatus = $response->getHeader(RedirectMiddleware::STATUS_HISTORY_HEADER);
        foreach ($historyUri as $key => $uri) {
          // Status and URI history share the same keys.
          $destination = $uri;
          $status = $historyStatus[$key];
        }
      }
    }

    if ($error) {
      $errorCount++;
    }
    else {
      $errorCount = 0;
    }

    $link->setLastCheckedTime($requestTime);
    $link->http_method = $method;
    $link->http_status = $status;
    $link->destination = $destination;
    $link->message = $message;
    $link->error = $error;
    $link->error_count = $errorCount;
    $link->internal = !$url->isExternal();
    $link->save();

    $this->logger->info('Finished crawling link @id', [
      '@id' => $link->id(),
    ]);
  }

  /**
   * Request a URL.
   *
   * @param string $uri
   *   The URI.
   * @param string $method
   *   The HTTP method.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   A response.
   *
   * @throws \Exception
   *   Various Guzzle exceptions.
   *
   * @internal The arguments, operation, and return values are subject to
   *   change.
   */
  private function request($uri, $method) {
    $linkyCheckerSettings = $this->configFactory->get('linkychecker.settings');
    $userAgent = $linkyCheckerSettings->get('user_agent');
    assert(is_string($userAgent));

    $options = [
      $method,
      $uri,
      [
        'headers' => [
          'User-Agent' => $userAgent,
        ],
        // http://docs.guzzlephp.org/en/stable/request-options.html#allow-redirects
        'allow_redirects' => [
          'max'             => 5,
          'protocols'       => ['http', 'https'],
          'track_redirects' => TRUE,
          'referer' => FALSE,
        ],
        // Don't worry about invalid SSL certs.
        'verify' => FALSE,
      ],
    ];

    return $this->httpClient->request(...$options);
  }

}
