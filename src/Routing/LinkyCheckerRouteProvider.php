<?php

declare(strict_types = 1);

namespace Drupal\linkychecker\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\EntityRouteProviderInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Route provider for Linkychecker.
 */
class LinkyCheckerRouteProvider implements EntityRouteProviderInterface {

  public const LINK_TEMPLATE_CRAWL = 'linkychecker-crawl';

  public const LINK_TEMPLATE_INCLUDE = 'linkychecker-include';

  public const LINK_TEMPLATE_EXCLUDE = 'linkychecker-exclude';

  public const ROUTE_NAME_CRAWL = 'entity.linky.linkychecker_crawl';

  public const ROUTE_NAME_INCLUDE = 'entity.linky.linkychecker_include';

  public const ROUTE_NAME_EXCLUDE = 'entity.linky.linkychecker_exclude';

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = new RouteCollection();

    $route = (new Route('/admin/content/linky/{linky}/crawl'))
      ->addDefaults([
        '_title' => 'Crawl',
        '_entity_form' => 'linky.linkychecker-crawl',
      ])
      ->setRequirement('_entity_access', 'linky.update')
      ->setOption('parameters', [
        'linky' => ['type' => 'entity:linky'],
      ]);
    $collection->add(self::ROUTE_NAME_CRAWL, $route);

    $route = (new Route('/admin/content/linky/{linky}/exclude'))
      ->addDefaults([
        '_title' => 'Exclude Link',
        '_entity_form' => 'linky.linkychecker-exclude',
      ])
      ->setRequirement('_entity_access', 'linky.update')
      ->setOption('parameters', [
        'linky' => ['type' => 'entity:linky'],
      ]);
    $collection->add(self::ROUTE_NAME_EXCLUDE, $route);

    $route = (new Route('/admin/content/linky/{linky}/include'))
      ->addDefaults([
        '_title' => 'Remove Exclusion Link',
        '_entity_form' => 'linky.linkychecker-include',
      ])
      ->setRequirement('_entity_access', 'linky.update')
      ->setOption('parameters', [
        'linky' => ['type' => 'entity:linky'],
      ]);
    $collection->add(self::ROUTE_NAME_INCLUDE, $route);

    return $collection;
  }

}
