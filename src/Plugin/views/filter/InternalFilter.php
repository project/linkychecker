<?php

namespace Drupal\linkychecker\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\BooleanOperator;

/**
 * Overrides boolean filter adding appropriate labels for internal/external.
 *
 * @property array valueOptions
 * @ViewsFilter("internal_external_filter")
 */
class InternalFilter extends BooleanOperator {

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {
    $this->valueOptions = [
      1 => $this->t('Internal'),
      0 => $this->t('External'),
    ];
  }

}
