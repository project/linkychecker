<?php

namespace Drupal\linkychecker\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\linkychecker\LinkyCheckerCrawlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Updates a feed's items.
 *
 * @QueueWorker(
 *   id = "linkychecker_crawl_job",
 *   title = @Translation("Linkychecker Crawl Job"),
 *   cron = {"time" = 60}
 * )
 */
class CrawlJob extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The linky checker crawler.
   *
   * @var \Drupal\linkychecker\LinkyCheckerCrawlerInterface
   */
  protected $crawler;

  /**
   * Storage for linky entities.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $linkyStorage;

  /**
   * Constructs a new CrawlJob.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\linkychecker\LinkyCheckerCrawlerInterface $linkyCheckerCrawler
   *   The linky checker crawler.
   * @param \Drupal\Core\Entity\EntityStorageInterface $linkyStorage
   *   Storage for linky entities.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, LinkyCheckerCrawlerInterface $linkyCheckerCrawler, EntityStorageInterface $linkyStorage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->crawler = $linkyCheckerCrawler;
    $this->linkyStorage = $linkyStorage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('linkychecker.crawler'),
      $container->get('entity_type.manager')->getStorage('linky')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    /** @var int[] $ids */
    $ids = $data['linky_ids'];
    foreach ($this->linkyStorage->loadMultiple($ids) as $linky) {
      /** @var \Drupal\linky\LinkyInterface $linky */
      $this->crawler->checkLinks([$linky]);
    }
  }

}
