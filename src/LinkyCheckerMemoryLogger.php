<?php

declare(strict_types = 1);

namespace Drupal\linkychecker;

use Psr\Log\LoggerInterface;
use Psr\Log\LoggerTrait;

/**
 * Defines a logger channel that most implementations will use.
 */
class LinkyCheckerMemoryLogger implements LoggerInterface {

  use LoggerTrait;

  /**
   * Logs.
   *
   * @var array
   */
  protected $logs = [];

  /**
   * {@inheritdoc}
   */
  public function log($level, string|\Stringable $message, array $context = []): void {
    $this->logs[] = [$level, $message, $context];
  }

  /**
   * Get the logged messages.
   *
   * @return array
   *   An array of logs.
   */
  public function getLogs(): array {
    return $this->logs;
  }

}
