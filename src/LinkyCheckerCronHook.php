<?php

namespace Drupal\linkychecker;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides cron hook for Linkychecker.
 */
class LinkyCheckerCronHook implements ContainerInjectionInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The linky checker queue service.
   *
   * @var \Drupal\linkychecker\LinkyCheckerCrawlQueueInterface
   */
  protected $checkerCrawlQueue;

  /**
   * Constructs a new LinkyCheckerCronHook.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\linkychecker\LinkyCheckerCrawlQueueInterface $checkerCrawlQueue
   *   The linky checker queue service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, LinkyCheckerCrawlQueueInterface $checkerCrawlQueue) {
    $this->configFactory = $configFactory;
    $this->checkerCrawlQueue = $checkerCrawlQueue;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('linkychecker.crawl_queue')
    );
  }

  /**
   * Implements hook_cron().
   *
   * @see linkychecker_cron()
   */
  public function cron(): void {
    $enableCron = $this->configFactory->get('linkychecker.settings')->get('enable_cron');
    if ($enableCron) {
      $this->checkerCrawlQueue->jobCreator();
    }
  }

}
