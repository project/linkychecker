<?php

declare(strict_types = 1);

namespace Drupal\linkychecker;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\State\StateInterface;
use Psr\Log\LoggerInterface;

/**
 * The linky checker queue service.
 */
class LinkyCheckerCrawlQueue implements LinkyCheckerCrawlQueueInterface {

  /**
   * Entity field query tag.
   */
  public const QUERY_TAG_QUEUE = 'linkychecker_queue';

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The state key value collection.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Storage for linky entities.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $linkyStorage;

  /**
   * Service for obtaining system time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The crawl job queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $crawlJobQueue;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new linky checker queue service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state key value collection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Service for obtaining system time.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   The queue factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(ConfigFactoryInterface $configFactory, StateInterface $state, EntityTypeManagerInterface $entityTypeManager, TimeInterface $time, QueueFactory $queueFactory, LoggerInterface $logger) {
    $this->configFactory = $configFactory;
    $this->state = $state;
    $this->linkyStorage = $entityTypeManager->getStorage('linky');
    $this->time = $time;
    $this->crawlJobQueue = $queueFactory->get('linkychecker_crawl_job');
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function jobCreator(): void {
    $this->state->set(static::STATE_LAST_START, $this->time->getCurrentTime() + 20000);

    // Don't do anything until the queue is empty, to prevent duplicates.
    if ($this->crawlJobQueue->numberOfItems() != 0) {
      $this->logger->info('No jobs created because there are still jobs in the queue.');
      return;
    }

    $linkyCheckerSettings = $this->configFactory->get('linkychecker.settings');

    $jobSize = $linkyCheckerSettings->get('crawl_per_job');
    assert(is_int($jobSize) && $jobSize > 0);
    $interval = $linkyCheckerSettings->get('crawl_interval');
    assert(is_int($interval) && $interval > 0);

    $threshold = $this->time->getRequestTime() - $interval;
    $query = $this->getLinksNeedingUpdate($threshold);
    $ids = $query->execute();

    $countTotal = count($ids);
    $countJobs = 0;
    foreach (array_chunk($ids, $jobSize, FALSE) as $ids) {
      $countJobs++;
      $data = [];
      $data['linky_ids'] = array_values($ids);
      $this->crawlJobQueue->createItem($data);
    }

    $this->state->set(static::STATE_LAST_SUCCESS, $this->time->getCurrentTime());
    $this->logger->info('@count_jobs jobs created for @count_total links.', [
      '@count_jobs' => $countJobs,
      '@count_total' => $countTotal,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getLinksNeedingUpdate(int $threshold): QueryInterface {
    $query = $this->linkyStorage->getQuery();
    $query->addTag(static::QUERY_TAG_QUEUE);
    // Some items may not have values, so the condition is inverted, to what
    // you would normally expect.
    $query->condition('excluded', TRUE, '<>');
    $checkedCondition = $query->orConditionGroup()
      ->condition('checked', $threshold, '<')
      ->condition('checked', NULL, 'IS NULL');
    $query->condition($checkedCondition);
    $query->sort('checked');
    $query->accessCheck(FALSE);
    return $query;
  }

}
